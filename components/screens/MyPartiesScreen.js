import React from 'react';
import { View, ScrollView, StyleSheet, Text, TouchableWithoutFeedback } from 'react-native';
import { Icon, Card, ListItem } from 'react-native-elements';
import firebase from 'firebase';

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  fonts: {
    marginBottom: 8,
  },
});

class PartyCard extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      name: this.props.name,
      time: this.props.time,
      location: this.props.location,
      creator: this.props.creator,
      screenName: '',
      avatar: '',
    }
    this.timeConverter = this.timeConverter.bind(this);
  }

  componentDidMount() {
    firebase.database().ref('users/' + this.state.creator).once('value', (snapshot) => {
      var user = snapshot.val();
      this.setState({
        screenName: user.screenName,
        avatar: user.photoURL,
      });
    });
  }

  timeConverter(timestamp) {
    var a = new Date(timestamp);
    var months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
    var year = a.getFullYear();
    var month = months[a.getMonth()];
    var date = a.getDate();
    var hour = a.getHours();
    var min = a.getMinutes();
    if (hour < 10) {
      hour = '0' + hour;
    }
    if (min < 10) {
      min = '0' + min;
    }
    var time = date + ' ' + month + ' ' + year + '  ' + hour + ':' + min ;
    return time;
  }

  render() {
    var avatar;
    if (this.state.avatar == '') {
      avatar = { rounded: true, icon: {name: 'account', type: 'material-community'} };
    } else {
      avatar = { rounded: true, source: { uri: this.state.avatar } };
    }

    return (
      <TouchableWithoutFeedback
        onPress={async () => {
          this.props.navigation.navigate('PartyDetails', {id: this.props.id});
        }}
      >
        <Card
          title={this.state.name}
          titleStyle={{ color: '#05A255', fontSize: 18 }}
          containerStyle={{
            marginTop: 15,
            borderRadius: 4,
            borderWidth: 1,
            borderColor: 'white',
          }}
        >
          <ListItem
            title={this.state.screenName}
            subtitle="Creator"
            leftAvatar={avatar}
            titleStyle={{ fontWeight: 'bold' }}
            containerStyle={{ marginLeft: -15, }}
          />
          <View style={{ flexDirection: 'row', marginLeft: 5 }}>
            <Text style={{ marginBottom: 8 }}>Date: </Text>
            <Text
              style={[
                styles.fonts,
                {
                  marginBottom: 8,
                  marginLeft: 5,
                  color: 'rgba(0, 0, 0, 0.54)',
                },
              ]}
            >
              {this.timeConverter(this.state.time)}
            </Text>
          </View>
          <View style={{ flexDirection: 'row', marginLeft: 5 }}>
            <Text style={{ marginBottom: 8 }}>Location: </Text>
            <Text
              style={[
                styles.fonts,
                {
                  marginBottom: 8,
                  marginLeft: 5,
                  color: 'rgba(0, 0, 0, 0.54)',
                },
              ]}
            >
              {this.state.location}
            </Text>
          </View>
        </Card>
      </TouchableWithoutFeedback>
    )
  }
}

class MyPartiesScreen extends React.Component {
  static navigationOptions = ({ navigation }) => ({
    title: 'My Parties',
    headerLeft: (
      <Icon
        name="navicon"
        size={30}
        type="evilicon"
        iconStyle={{ paddingLeft: 10 }}
        onPress={() => {
          navigation.openDrawer();
        }}
      />
    ),
    headerRight: (
      <Icon
        name="plus"
        size={40}
        type="evilicon"
        iconStyle={{ paddingRight: 10 }}
        onPress={async () => {
          navigation.navigate('EditParty', {id: ''});
        }}
      />
    ),
  });

  constructor(props) {
    super(props);
    this.state = {
      cards: [],
    };
  }

  componentDidMount() {
    var user = firebase.auth().currentUser;
    firebase.database().ref('users/' + user.uid + '/parties').on('value', this.getCards.bind(this));
  }

  getCards() {
    var user = firebase.auth().currentUser;
    firebase.database().ref('users/' + user.uid).once('value', (snapshot) => {
      this.setState({
        cards: [],
      });
      if (snapshot.val().parties != null) {
        snapshot.val().parties.map((id) => {
          firebase.database().ref('parties/' + id).once('value', (snapshot) => {
            var partyDetails = snapshot.val();
            var newCards = this.state.cards.concat(
              <PartyCard
                key={id}
                id={id}
                name={partyDetails.name}
                creator={partyDetails.creator}
                time={partyDetails.time}
                location={partyDetails.location}
                navigation={this.props.navigation}
              />
            );
            this.setState({
              cards: newCards,
            });
          });
        });
      };
    });
  }

  render() {
    return (
      <ScrollView>
        <View style={styles.container}>
          {this.state.cards}
        </View>
      </ScrollView>
    );
  }
}

export default MyPartiesScreen;
