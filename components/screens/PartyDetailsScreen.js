import React, { Component } from 'react';
import {
  ScrollView,
  View,
  StyleSheet,
  Dimensions,
  Picker,
  Switch,
  Alert,
} from 'react-native';
import {
  Card,
  Text,
  ListItem,
  Button,
} from 'react-native-elements';
import firebase from 'firebase';

const SCREEN_WIDTH = Dimensions.get('window').width;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  cardTitle: {
  	color: 'orange',
  	fontSize: 18,
  },
  cardContainer: {
  	marginTop: 15,
    borderRadius: 4,
    borderWidth: 1,
    borderColor: 'white',
  },
  avatar: {
    alignSelf: 'center',
    marginVertical: 5,
  },
  buttonContainer: {
    height: 50,
    width: 100,
    backgroundColor: 'orange',
    borderWidth: 1,
    borderColor: 'orange',
    borderRadius: 40,
    alignSelf: 'center',
    marginVertical: 5,
  },
});

class ParticipantListItem extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      key: props.id,
      avatar: props.avatar,
      name: props.name,
      hobbies: props.hobbies,
    };
  }

  render() {
    var avatar;
    if (this.state.avatar == null || this.state.avatar == '') {
      avatar = { rounded: true, icon: {name: 'account', type: 'material-community'} };
    } else {
      avatar = { rounded: true, source: { uri: this.state.avatar } };
    }
    if (this.state.hobbies != '') {
      return (
        <ListItem
          key={this.state.key}
          leftAvatar={avatar}
          title={this.state.name}
          subtitle={this.state.hobbies}
          onPress={async () => {
            this.props.navigation.navigate('ProfileDetails', {id: this.props.id});
          }}
        />
      );
    } else {
      return (
        <ListItem
          key={this.state.key}
          leftAvatar={avatar}
          title={this.state.name}
          onPress={async () => {
            this.props.navigation.navigate('ProfileDetails', {id: this.props.id});
          }}
        />
      );
    }
  }
}

export default class PartyDetailsScreen extends React.Component {
  static navigationOptions = {
    title: 'View Party',
  };

  constructor(props) {
    super(props);
    this.state = {
      id: this.props.navigation.getParam('id', ''),
      name: '',
      description: '',
      location: '',
      time: new Date(),
      limit: 2,
      creator: '',
      participants: [],
    };
    this.timeConverter = this.timeConverter.bind(this);
  }

  componentDidMount() {
    if (this.state.id != '') {
      firebase.database().ref('parties/' + this.state.id).on('value', this.getParty.bind(this));
    }
  }

  timeConverter(timestamp) {
    var a = new Date(timestamp);
    var months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
    var year = a.getFullYear();
    var month = months[a.getMonth()];
    var date = a.getDate();
    var hour = a.getHours();
    var min = a.getMinutes();
    if (hour < 10) {
      hour = '0' + hour;
    }
    if (min < 10) {
      min = '0' + min;
    }
    var time = date + ' ' + month + ' ' + year + '  ' + hour + ':' + min ;
    return time;
  }

  joinParty() {
    var user = firebase.auth().currentUser;

    if (user.uid != this.state.creator) {
      firebase.database().ref('parties/' + this.state.id).once('value', (snapshot) => {
        var data = snapshot.val();
        var limit = data.limit;
        var participantArray = data.participants;
        if (participantArray != null) {
          if (participantArray.length >= limit) {
            Alert.alert("Party full");
          } else {
            var userIndex = participantArray.indexOf(user.uid);
            if (userIndex == -1) {
              firebase.database().ref('parties/' + this.state.id).update({
                participants: participantArray.concat(user.uid),
              });
              firebase.database().ref('users/' + user.uid + '/parties').once('value', (snapshot) => {
                var partyArray = snapshot.val();
                if (partyArray == null) {
                  partyArray = [];
                }
                firebase.database().ref('users/' + user.uid).update({
                  parties: partyArray.concat(this.state.id),
                });
              });
            }
          }
        }
      });
    }
  }

  leaveParty() {
    var user = firebase.auth().currentUser;

    if (user.uid != this.state.creator) {
      firebase.database().ref('parties/' + this.state.id + '/participants').once('value', (snapshot) => {
        var participantArray = snapshot.val();
        if (participantArray != null) {
          var userIndex = participantArray.indexOf(user.uid);
          if (userIndex != -1) {
            participantArray.splice(userIndex, 1);
            firebase.database().ref('parties/' + this.state.id).update({
              participants: participantArray,
            });
          }
        }
        firebase.database().ref('users/' + user.uid + '/parties').once('value', (snapshot) => {
          var partyArray = snapshot.val();
          if (partyArray != null) {
            var partyIndex = partyArray.indexOf(this.state.id);
            if (partyIndex != -1) {
              partyArray.splice(partyIndex, 1);
              firebase.database().ref('users/' + user.uid).update({
                parties: partyArray,
              });
            }
          }
        });
      });
    }
  }

  getParty() {
    firebase.database().ref('parties/' + this.state.id).once('value', (snapshot) => {
      var party = snapshot.val();
      this.setState({
        name: party.name,
        description: party.description,
        location: party.location,
        time: new Date(party.time),
        limit: party.limit,
        participants: [],
        creator: party.creator,
      });
      var participantArray = party.participants;
      if (participantArray != null) {
        participantArray.forEach(x => {
          firebase.database().ref('users/' + x).once('value', (snapshot) => {
            var user = snapshot.val();
            if (user != null) {
              var newParticipants = this.state.participants.concat({
                id: x,
                name: user.screenName,
                avatar: user.photoURL,
                hobbies: user.hobbies,
              });
              this.setState({
                participants: newParticipants,
              });
            }
          });
        });
      }
    });
  }

  render() {
    var actionButton = (
      <Button
        buttonStyle={styles.buttonContainer}
        title=""
      />
    );
    var user = firebase.auth().currentUser;
    if (this.state.participants.length > 0) {
      if (this.state.creator == user.uid) {
        actionButton = (
          <Button
            buttonStyle={styles.buttonContainer}
            title="Edit"
            onPress={async () => {
              this.props.navigation.navigate('EditParty', {id: this.state.id});
            }}
          />
        );
      } else if (this.state.participants.findIndex(x => {
        return x.id == user.uid;
      }) == -1) {
        actionButton = (
          <Button
            buttonStyle={styles.buttonContainer}
            title="Join"
            onPress={this.joinParty.bind(this)}
          />
        );
      } else {
        actionButton = (
          <Button
            buttonStyle={styles.buttonContainer}
            title="Leave"
            onPress={this.leaveParty.bind(this)}
          />
        );
      }
    }

    var participantListItems = [];
    this.state.participants.forEach(x => {
      participantListItems.push(
        <ParticipantListItem
          key={x.id}
          id={x.id}
          name={x.name}
          hobbies={x.hobbies}
          avatar={x.avatar}
          navigation={this.props.navigation}
        />
      );
    });

    return (
      <ScrollView>
        <View style={styles.container}>
          <Card
            title="Party Details"
            titleStyle={styles.cardTitle}
            containerStyle={styles.cardContainer}
          >
            <ListItem
              title="Name"
              subtitle={this.state.name}
            />
            <ListItem
              title="Description"
              subtitle={this.state.description}
            />
            <ListItem
              title="Location"
              subtitle={this.state.location}
            />
            <ListItem
              title="Group Size"
              subtitle={this.state.limit.toString()}
            />
            <ListItem
              title="Time"
              subtitle={this.timeConverter(this.state.time.getTime())}
            />
            {actionButton}
          </Card>
          <Card
            title="Participants"
            titleStyle={styles.cardTitle}
            containerStyle={styles.cardContainer}
          >
            {participantListItems}
          </Card>
        </View>
      </ScrollView>
    );
  }
}
