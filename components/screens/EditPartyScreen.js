import React, { Component } from 'react';
import {
  ScrollView,
  View,
  StyleSheet,
  Dimensions,
  Picker,
  Switch,
  DatePickerIOS,
} from 'react-native';
import {
  Avatar,
  Card,
  Text,
  ListItem,
  Input,
  Button,
} from 'react-native-elements';
import ImagePicker from 'react-native-image-crop-picker';
import firebase from 'firebase';

const SCREEN_WIDTH = Dimensions.get('window').width;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  cardTitle: {
  	color: 'orange',
  	fontSize: 18,
  },
  cardContainer: {
  	marginTop: 15,
    borderRadius: 4,
    borderWidth: 1,
    borderColor: 'white',
  },
  avatar: {
    alignSelf: 'center',
    marginVertical: 5,
  },
  inputContainer: {
  	borderRadius: 40,
    borderWidth: 1,
    borderColor: 'rgba(110, 120, 170, 1)',
    height: 50,
    
    alignSelf: 'center',
    marginVertical: 5,
    paddingLeft: 10,
    paddingRight: 10,
  },
  inputLabel: {
  	fontSize: 16,
  	marginLeft: 5,
  	marginTop: 5,
  },
  saveButtonContainer: {
  	height: 50,
    width: 100,
    backgroundColor: 'orange',
    borderWidth: 1,
    borderColor: 'orange',
    borderRadius: 40,
    alignSelf: 'center',
    marginVertical: 5,
    marginTop: 15,
  },
  imageButtonContainer: {
    height: 50,
    width: 150,
    backgroundColor: 'orange',
    borderWidth: 1,
    borderColor: 'orange',
    borderRadius: 40,
    alignSelf: 'center',
    marginVertical: 5,
    marginTop: 15,
  },
});

export default class EditPartyScreen extends Component {
  static navigationOptions = {
    title: 'Edit Party',
  };

  constructor(props) {
    super(props);
    this.state = {
      notifyParty: false,
      id: this.props.navigation.getParam('id', ''),
      name: '',
      image: { uri: null },
      imageChanged: false,
      description: '',
      location: '',
      time: new Date(),
      limit: 2,
    };
    this.timeConverter = this.timeConverter.bind(this);
  }

  componentDidMount() {
    if (this.state.id != '') {
      firebase.database().ref('parties/' + this.state.id).once('value', (snapshot) => {
        var partyDetails = snapshot.val();
        this.setState({
          name: partyDetails.name,
          image: { uri: partyDetails.photoURL },
          imageChanged: false,
          description: partyDetails.description,
          location: partyDetails.location,
          time: new Date(partyDetails.time),
          limit: partyDetails.limit,
        });
      });
    };
  }

  timeConverter(timestamp) {
    var a = new Date(timestamp);
    var months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
    var year = a.getFullYear();
    var month = months[a.getMonth()];
    var date = a.getDate();
    var hour = a.getHours();
    var min = a.getMinutes();
    var time = date + ' ' + month + ' ' + year + '  ' + hour + ':' + min ;
    return time;
  }

  savePartyDetails() {
    var user = firebase.auth().currentUser;
    var id = this.state.id;

    if (id == '') {
      id = firebase.database().ref('parties').push({
        creator: user.uid,
        name: this.state.name,
        description: this.state.description,
        photoURL: '',
        location: this.state.location,
        limit: this.state.limit,
        time: this.state.time.getTime(),
        participants: [user.uid],
      }).key;

      firebase.database().ref('users/' + user.uid + '/parties').once('value', (snapshot) => {
        var partyArray = snapshot.val();
        if (partyArray == null) {
          partyArray = [];
        }
        firebase.database().ref('users/' + user.uid).update({
          parties: partyArray.concat(id),
        });
      });
    } else {
      firebase.database().ref('parties/' + id).update({
        name: this.state.name,
        description: this.state.description,
        location: this.state.location,
        limit: this.state.limit,
        time: this.state.time.getTime(),
      });
    }

    if (this.state.imageChanged) {
      var storageRef = firebase.storage().ref('parties/' + id + '/image');

      fetch(this.state.image.uri).then(res => res.blob()).then(blob => {
        storageRef.put(blob).then(() => {
          return storageRef.getDownloadURL();
        }).then((url) => {
          firebase.database().ref('parties/' + id).update({
            photoURL: url,
          })
        });
      });
      this.setState({
        imageChanged: false,
      });
    }

    const { navigation } = this.props;
    navigation.goBack();
  }

  render() {
    var image;
    if (this.state.image.uri == null || this.state.image.uri == '') {
      image = (
        <Button
          buttonStyle={styles.imageButtonContainer}
          title="Pick Image"
          onPress={() => ImagePicker.openPicker(
            {
              width: 200,
              height: 200,
              cropping: false
            }).then(image => {
              this.setState({
                image: { uri: image.sourceURL },
                imageChanged: true,
              });
            }).catch(e => console.log(e))
          }
        />
      );
    } else {
      image = (
        <Avatar
          size="xlarge"
          rounded
          source={this.state.image}
          containerStyle={styles.avatar}
          onPress={() => ImagePicker.openPicker(
            {
              width: 200,
              height: 200,
              cropping: false
            }).then(image => {
              this.setState({
                image: { uri: image.sourceURL },
                imageChanged: true,
              });
            }).catch(e => console.log(e))
          }
        />
      );
    }

    return (
      <ScrollView>
        <View style={styles.container}>
          <Card
            title="Party Details"
            titleStyle={styles.cardTitle}
            containerStyle={styles.cardContainer}
          >
            {image}
            <Input
              inputContainerStyle={styles.inputContainer}
              label="Name"
              labelStyle={styles.inputLabel}
              value={this.state.name}
              onChangeText={name => this.setState({ name: name })}
            />
            <Input
              inputContainerStyle={styles.inputContainer}
              label="Description"
              labelStyle={styles.inputLabel}
              value={this.state.description}
              onChangeText={description => this.setState({ description: description })}
            />
            <Input
              inputContainerStyle={styles.inputContainer}
              label="Location"
              labelStyle={styles.inputLabel}
              value={this.state.location}
              onChangeText={location => this.setState({ location: location })}
            />
            <Input
              inputContainerStyle={styles.inputContainer}
              label="Group Size"
              labelStyle={styles.inputLabel}
              value={this.state.limit.toString()}
              keyboardType="number-pad"
              onChangeText={limit => {
                if (limit != '') {
                  var newInt = parseInt(limit, 10);
                  if (!isNaN(newInt)) {
                    this.setState({ limit: newInt });
                  }
                } else {
                  this.setState({ limit: 0 });
                }
              }}
            />
            <Card
              title="Time"
            >
            <DatePickerIOS
              date={this.state.time}
              onDateChange={time => this.setState({ time: time })}
            />
            </Card>
            <Button
            	buttonStyle={styles.saveButtonContainer}
  						title="Save"
              onPress={this.savePartyDetails.bind(this)}
						/>
          </Card>
        </View>
      </ScrollView>
    );
  }
}
