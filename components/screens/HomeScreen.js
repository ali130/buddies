import React, { Component } from 'react';
import {
  ScrollView,
  View,
  StyleSheet,
} from 'react-native';
import {
  Icon,
  Card,
  Text,
  ListItem,
} from 'react-native-elements';
import FlatListComponent from '../FlatListComponent';

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  fonts: {
    marginBottom: 8,
  },
});

const AVATAR_IMAGE = require('../../assets/images/avatar.jpg');

export default class HomeScreen extends Component {
  static navigationOptions = ({ navigation }) => ({
    title: 'Home',
    headerLeft: (
      <Icon
        name="navicon"
        size={30}
        type="evilicon"
        iconStyle={{ paddingLeft: 10 }}
        onPress={() => {
          navigation.openDrawer();
        }}
      />
    ),
  });

  render() {
    return (
        <FlatListComponent></FlatListComponent>
    );
  }
}
