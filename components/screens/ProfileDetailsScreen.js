import React, { Component } from 'react';
import {
  ScrollView,
  View,
  StyleSheet,
  Dimensions,
  Picker,
  Switch,
} from 'react-native';
import {
  Avatar,
  Card,
  Text,
  ListItem,
} from 'react-native-elements';
import firebase from 'firebase';

const SCREEN_WIDTH = Dimensions.get('window').width;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  cardTitle: {
  	color: 'orange',
  	fontSize: 18,
  },
  cardContainer: {
  	marginTop: 15,
    borderRadius: 4,
    borderWidth: 1,
    borderColor: 'white',
  },
  avatar: {
    alignSelf: 'center',
    marginVertical: 5,
  },
});

export default class ProfileDetailsScreen extends React.Component {
  static navigationOptions = {
    title: 'View Profile',
  };

  constructor(props) {
    super(props);
    this.state = {
      id: this.props.navigation.getParam('id', ''),
      avatar: { uri: null },
      screenName: '',
      email: '',
      phone: '',
      hobbies: '',
      description: '',
    };
  }

  componentDidMount() {
    if (this.state.id != '') {
      firebase.database().ref('users/' + this.state.id).on('value', this.getUser.bind(this));
    }
  }

  getUser() {
    firebase.database().ref('users/' + this.state.id).once('value', (snapshot) => {
      var user = snapshot.val();
      if (user != null) {
        this.setState({
          avatar: { uri: user.photoURL },
          screenName: user.screenName,
          email: user.email,
          phone: user.phone,
          hobbies: user.hobbies,
          description: user.description,
        });
      }
    });
  }

  render() {
    var avatar;
    if (this.state.avatar.uri == null || this.state.avatar.uri == '') {
      avatar = (
        <Avatar
          size="xlarge"
          rounded
          icon={{name: 'account', type: 'material-community'}}
          containerStyle={styles.avatar}
        />
      );
    } else {
      avatar = (
        <Avatar
          size="xlarge"
          rounded
          source={this.state.avatar}
          containerStyle={styles.avatar}
        />
      );
    }

    return (
      <ScrollView>
        <View style={styles.container}>
          <Card
            title="User Details"
            titleStyle={styles.cardTitle}
            containerStyle={styles.cardContainer}
          >
            {avatar}
            <ListItem
              title="Screen Name"
              subtitle={this.state.screenName}
            />
            <ListItem
              title="Email Address"
              subtitle={this.state.email}
            />
            <ListItem
              title="Phone"
              subtitle={this.state.phone}
            />
            <ListItem
              title="Hobbies"
              subtitle={this.state.hobbies}
            />
            <ListItem
              title="Description"
              subtitle={this.state.description}
            />
          </Card>
        </View>
      </ScrollView>
    );
  }
}
