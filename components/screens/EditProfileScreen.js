import React, { Component } from 'react';
import {
  ScrollView,
  View,
  StyleSheet,
  Dimensions,
  Picker,
  Switch,
} from 'react-native';
import {
  Avatar,
  Card,
  Text,
  ListItem,
  Input,
  Button,
  Icon,
} from 'react-native-elements';
import ImagePicker from 'react-native-image-crop-picker';
import firebase from 'firebase';

const SCREEN_WIDTH = Dimensions.get('window').width;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  cardTitle: {
  	color: 'orange',
  	fontSize: 18,
  },
  cardContainer: {
  	marginTop: 15,
    borderRadius: 4,
    borderWidth: 1,
    borderColor: 'white',
  },
  avatar: {
    alignSelf: 'center',
    marginVertical: 5,
  },
  inputContainer: {
  	borderRadius: 40,
    borderWidth: 1,
    borderColor: 'rgba(110, 120, 170, 1)',
    height: 50,
    
    alignSelf: 'center',
    marginVertical: 5,
    paddingLeft: 10,
    paddingRight: 10,
  },
  inputLabel: {
  	fontSize: 16,
  	marginLeft: 5,
  	marginTop: 5,
  },
  saveButtonContainer: {
  	height: 50,
    width: 100,
    backgroundColor: 'orange',
    borderWidth: 1,
    borderColor: 'orange',
    borderRadius: 40,
    alignSelf: 'center',
    marginVertical: 5,
    marginTop: 15,
  },
});

export default class EditProfileScreen extends Component {
  static navigationOptions = ({ navigation }) => ({
    title: 'Profile',
    headerLeft: (
      <Icon
        name="navicon"
        size={30}
        type="evilicon"
        iconStyle={{ paddingLeft: 10 }}
        onPress={() => {
          navigation.openDrawer();
        }}
      />
    ),
  });

  constructor(props) {
    super(props);
    this.state = {
      notifyParty: false,
      avatar: { uri: null },
      avatarChanged: false,
      screenName: '',
      email: '',
      phone: '',
      hobbies: '',
      description: '',
    };
  }

  componentDidMount() {
    var user = firebase.auth().currentUser;

    firebase.database().ref('users/' + user.uid).once('value', (snapshot) => {
      var userDetails = snapshot.val();
      this.setState({
        avatar: { uri: user.photoURL },
        avatarChanged: false,
        screenName: user.displayName,
        email: user.email,
        phone: userDetails.phone,
        hobbies: userDetails.hobbies,
        description: userDetails.description,
      });
    });
  }

  saveUserDetails() {
    var user = firebase.auth().currentUser;
    var storageRef = firebase.storage().ref('users/' + user.uid + '/avatar');

    if (this.state.avatarChanged) {
      fetch(this.state.avatar.uri).then(res => res.blob()).then(blob => {
        storageRef.put(blob).then(() => {
          return storageRef.getDownloadURL();
        }).then((url) => {
          user.updateProfile({
            photoURL: url,
          });
          firebase.database().ref('users/' + user.uid).update({
            photoURL: url,
          })
        });
      });
      this.setState({
        avatarChanged: false,
      });
    }

    user.updateProfile({
      displayName: this.state.screenName,
      email: this.state.email,
    })

    firebase.database().ref('users/' + user.uid).update({
      screenName: this.state.screenName,
      email: this.state.email,
      phone: this.state.phone,
      hobbies: this.state.hobbies,
      description: this.state.description,
    });
  }

  render() {
    const { navigation } = this.props;
    const { notifyParty } = this.state;

    var avatar;
    if (this.state.avatar.uri == null || this.state.avatar.uri == '') {
      avatar = (
        <Avatar
          size="xlarge"
          rounded
          icon={{name: 'account', type: 'material-community'}}
          containerStyle={styles.avatar}
          onPress={() => ImagePicker.openPicker(
            {
              width: 200,
              height: 200,
              cropping: false
            }).then(image => {
              this.setState({
                avatar: { uri: image.sourceURL },
                avatarChanged: true,
              });
            }).catch(e => console.log(e))
          }
        />
      );
    } else {
      avatar = (
        <Avatar
          size="xlarge"
          rounded
          source={this.state.avatar}
          containerStyle={styles.avatar}
          onPress={() => ImagePicker.openPicker(
            {
              width: 200,
              height: 200,
              cropping: false
            }).then(image => {
              this.setState({
                avatar: { uri: image.sourceURL },
                avatarChanged: true,
              });
            }).catch(e => console.log(e))
          }
        />
      );
    }

    return (
      <ScrollView>
        <View style={styles.container}>
          <Card
            title="User Details"
            titleStyle={styles.cardTitle}
            containerStyle={styles.cardContainer}
          >
            {avatar}
            <Input
              inputContainerStyle={styles.inputContainer}
              label="Screen Name"
              labelStyle={styles.inputLabel}
              value={this.state.screenName}
              onChangeText={screenName => this.setState({ screenName: screenName })}
            />
            <Input
              inputContainerStyle={styles.inputContainer}
              label="Email Address"
              labelStyle={styles.inputLabel}
              value={this.state.email}
              onChangeText={email => this.setState({ email: email })}
            />
            <Input
              inputContainerStyle={styles.inputContainer}
              label="Phone"
              labelStyle={styles.inputLabel}
              value={this.state.phone}
              onChangeText={phone => this.setState({ phone: phone })}
            />
            <Input
              inputContainerStyle={styles.inputContainer}
              label="Hobbies"
              labelStyle={styles.inputLabel}
              value={this.state.hobbies}
              onChangeText={hobbies => this.setState({ hobbies: hobbies })}
            />
            <Input
              inputContainerStyle={styles.inputContainer}
              label="Description"
              labelStyle={styles.inputLabel}
              value={this.state.description}
              onChangeText={description => this.setState({ description: description })}
            />
            <Button
            	buttonStyle={styles.saveButtonContainer}
  						title="Save"
              onPress={this.saveUserDetails.bind(this)}
						/>
          </Card>
        </View>
      </ScrollView>
    );
  }
}
