import React, { Component } from 'react';
import {
  ActivityIndicator,
  AsyncStorage,
  StatusBar,
  StyleSheet,
  View,
} from 'react-native';
import firebase from 'firebase';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
});

export default class AuthLoadingScreen extends Component {
  componentDidMount() {
    // Initialize Firebase
    let config = {
      apiKey: "AIzaSyCzrxcbGvQnzMPYOz_AJYZ9R8-xcqOJopA",
      authDomain: "buddies-9c9e7.firebaseapp.com",
      databaseURL: "https://buddies-9c9e7.firebaseio.com",
      projectId: "buddies-9c9e7",
      storageBucket: "buddies-9c9e7.appspot.com",
      messagingSenderId: "769932943268"
    };
    firebase.initializeApp(config);

    // switch between Auth and App stacks based on the token
    const { navigation } = this.props;
    firebase.auth().onAuthStateChanged((user) => {
      if (user) {
        navigation.navigate('App');
      } else {
        navigation.navigate('Auth');
      }
    })
  }

  render() {
    return (
      <View style={styles.container}>
        <ActivityIndicator />
        <StatusBar barStyle="default" />
      </View>
    );
  }
}
