import React, { Component } from "react";
import { View, Text, FlatList, ActivityIndicator } from "react-native";
import { List, ListItem, SearchBar } from "react-native-elements";
import BuddiesListItem from "./BuddiesListItem";
import firebase from 'firebase';

export default class FlatListComponent extends Component {
  constructor(props) {
    super(props);

    this.state = {
      loading: false,
      data: [],
      page: 1,
      seed: 1,
      error: null,
      refreshing: false
    };
  }

  componentDidMount() {
    this.makeRemoteRequest();
  }

  makeRemoteRequest = () => {
    this.setState({
      data: [],
    });
    firebase.database().ref('parties').once('value', (snapshot) => {
      snapshot.forEach((childSnapshot) => {
        var partyId = childSnapshot.key;
        var partyDetails = childSnapshot.val();
        var newData = this.state.data.concat({
          id: partyId,
          image: { uri: partyDetails.photoURL },
          title: partyDetails.name,
          subtitle: partyDetails.description,
        });
        this.setState({
          data: newData,
        });
      });
    }).then(() => {
      this.setState({
        refreshing: false,
      });
    });
  };

  handleRefresh = () => {
    this.setState(
      {
        page: 1,
        seed: this.state.seed + 1,
        refreshing: true
      },
      () => {
        this.makeRemoteRequest();
      }
    );
  };

  handleLoadMore = () => {
    // this.setState(
    //   {
    //     page: this.state.page + 1
    //   },
    //   () => {
    //     this.makeRemoteRequest();
    //   }
    // );
  };

  renderSeparator = () => {
    return (
      <View
        style={{
          height: 1,
          width: "86%",
          backgroundColor: "#CED0CE",
          marginHorizontal: "5%"
        }}
      />
    );
  };

  // renderHeader = () => {
  //   return <SearchBar placeholder="Search Event..." lightTheme round />;
  // };

  renderFooter = () => {
    if (!this.state.loading) return null;

    return (
      <View
        style={{
          paddingVertical: 20,
          borderTopWidth: 1,
          borderColor: "#CED0CE"
        }}
      >
        <ActivityIndicator animating size="large" />
      </View>
    );
  };

  render() {
    return (
        <FlatList
          containerStyle={{ borderTopWidth: 0, borderBottomWidth: 0 }}
          data={this.state.data}
          renderItem={({ item }) => (
            <BuddiesListItem
              id={item.id}
              image={item.image}
              title={item.title}
              subtitle={item.subtitle}
              // source={{uri: item.picture.medium }}
            />
          )}
          keyExtractor={item => item.subtitle}
          ItemSeparatorComponent={this.renderSeparator}
          ListHeaderComponent={this.renderHeader}
          ListFooterComponent={this.renderFooter}
          onRefresh={this.handleRefresh}
          refreshing={this.state.refreshing}
          onEndReached={this.handleLoadMore}
          onEndReachedThreshold={0.5}
        />
    );
  }
}