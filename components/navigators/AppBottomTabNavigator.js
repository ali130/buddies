import React from 'react';
import {
  createBottomTabNavigator,
  createStackNavigator,
} from 'react-navigation';
import { Icon } from 'react-native-elements';

import EditProfileScreen from '../screens/EditProfileScreen';
import HomeScreen from '../screens/HomeScreen';
import MyPartiesScreen from '../screens/MyPartiesScreen';
import EditPartyScreen from '../screens/EditPartyScreen';
import PartyDetailsScreen from '../screens/PartyDetailsScreen';
import ProfileDetailsScreen from '../screens/ProfileDetailsScreen';

const PartiesStack = createStackNavigator({
  Parties: HomeScreen,
  PartyDetails: PartyDetailsScreen,
  ProfileDetails: ProfileDetailsScreen,
  EditParty: EditPartyScreen,
});

const MyPartiesStack = createStackNavigator({
  MyParties: MyPartiesScreen,
  PartyDetails: PartyDetailsScreen,
  ProfileDetails: ProfileDetailsScreen,
  EditParty: EditPartyScreen,
});

const ProfileStack = createStackNavigator({
  Profile: EditProfileScreen,
});

const AppBottomTabNavigator = createBottomTabNavigator(
  {
    'Parties': PartiesStack,
    'My Parties': MyPartiesStack,
    'Profile': ProfileStack,
  },
  {
    defaultNavigationOptions: ({ navigation }) => ({
      tabBarIcon: ({ focused, tintColor }) => {
        const { routeName } = navigation.state;
        let iconName;
        if (routeName === 'Parties') {
          iconName = `map-marker${focused ? '' : '-outline'}`;
        } else if (routeName === 'My Parties') {
          iconName = `star${focused ? '' : '-outline'}`;
        } else if (routeName === 'Profile') {
          iconName = `account-box${focused ? '' : '-outline'}`;
        }

        return <Icon type="material-community" name={iconName} size={25} color={tintColor} />;
      },
    }),
    tabBarOptions: {
      activeTintColor: '#F3182E',
      inactiveTintColor: '#05A255',
    },
    initialRouteName: 'Parties',
  },
);

export default AppBottomTabNavigator;
