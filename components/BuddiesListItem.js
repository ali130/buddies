import React, {Component} from "react";
import { View, ActivityIndicator, TouchableWithoutFeedback } from 'react-native';
import { ListItem, Image } from "react-native-elements";
import { withNavigation } from 'react-navigation';

class BuddiesListItem extends Component {
    constructor(props) {
        super(props);
    
        this.state = {
        };
      }

    render(){
        var image;
        if (this.props.image.uri == null || this.props.image.uri == '') {
            image = { uri: 'http://placeimg.com/1280/720/arch' };
        } else {
            image = this.props.image;
        }

        return(
            <TouchableWithoutFeedback
                onPress={async () => {
                    this.props.navigation.navigate('PartyDetails', {id: this.props.id});
                }}
            >
                <View
                    style={{
                        margin:10,
                        borderRadius: 10,
                        overflow:'hidden'
                    }}
                >
                    <Image 
                        source={image}
                        style={{
                            height: 200,
                            flex:1
                        }}
                        resizeMode='cover'
                        PlaceholderContent={<ActivityIndicator />}
                    />
                    <ListItem
                        roundAvatar
                        title={this.props.title}
                        titleStyle={{fontSize:30}}
                        subtitle={this.props.subtitle}
                        containerStyle={{ 
                            borderBottomWidth: 0
                        }}
                        rightTitle="View"
                        rightTitleStyle={{ color: "#1be278"}}
                    />
                </View>
            </TouchableWithoutFeedback>
        )
    }
}

export default withNavigation(BuddiesListItem);