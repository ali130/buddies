import React, { Component } from 'react';
import { createStore } from 'redux';
import { Provider } from 'react-redux';
import firebase from 'firebase';
 
import rootReducer from './reducers/rootReducer';
import Buddies from './AppWrapper';

const store = createStore(rootReducer);

export default class App extends Component {
  render() {
    return(
      <Provider store={store}>
        <Buddies />
      </Provider>
    )
  }
}
